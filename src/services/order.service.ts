import { CartEntity, OrderEntity } from "../models/models";
import * as CartRepository from "../repositories/cart.repository";
import * as OrderRepository from "../repositories/order.repository";
import { v4 as uuid } from 'uuid';

export const createAnOrder = async (userId: string): Promise<OrderEntity> => {
    try {
        const activeCart = await CartRepository.getCartByUserId(userId);

        if (!activeCart) {
            throw new Error('No cart');
        }
        if (activeCart.items.length === 0) {
            throw new Error('Cart is empty');
        }

        const newOrder: OrderEntity = {
            id: uuid(),
            userId: userId,
            cartId: activeCart.id,
            items: activeCart.items,
            payment: {
                type: 'card',
                address: 'random',
                creditCard: '1111-1111-1111-1111',
            },
            delivery: {
                type: 'post',
                address: 'random',
            },
            comments: '',
            status: 'created',
            total: calculateOrderTotal(activeCart),
        }

        return await OrderRepository.saveOrder(newOrder);
    } catch (error) {
        throw error;
    }
};

const calculateOrderTotal = (cart: CartEntity): number => {
    return cart.items.reduce((total, item) => {
        const productTotal = item.product.price * item.count;
        return total + productTotal;
    }, 0);
}