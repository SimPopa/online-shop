import { Product } from "../entities/Product";
import { ProductEntity } from "../models/models";
import * as ProductRepository from "../repositories/product.repository";

export const getSingleProduct = async (productId: string): Promise<Product | null> => { 
    return await ProductRepository.getProductById(productId);
};

export const getProductsList = async (): Promise<ProductEntity[]> => {
    return await ProductRepository.getProducts();
};