import { getRepository } from 'typeorm';
import { CartEntity, CartItemEntity, CartUpdateItemEntity } from '../models/models';
import * as CartRepository from '../repositories/cart.repository';
import * as ProductService from './product.service';
import { Cart } from '../entities/Cart';
import { Product } from '../entities/Product';
import { CartItem } from '../entities/CartItem';
import { AppDataSource } from '../data-source';

export const getOrCreateUserCart = async (userId: string): Promise<CartEntity> => {
	try {
		let existingCart: CartEntity | null = await CartRepository.getCartByUserId(userId);

		if (!existingCart) {
			return await CartRepository.addCart(userId);
		}

		return existingCart;
	} catch (error) {
		throw error;
	}
};

export const updateCart = async (userId: string, cartItem: CartUpdateItemEntity): Promise<CartEntity | null> => {
	try {
		const cartRepository = AppDataSource.getRepository(Cart);
		const cart = await cartRepository.findOne({ where: { userId }, relations: ['items', 'items.product'] });

		if (!cart) {
			throw new Error('Cart not found');
		}

		const productRepository = AppDataSource.getRepository(Product);
		const product = await productRepository.findOne({ where: { id: cartItem.productId } });
		if (!product) {
			throw new Error('Product not found');
		}

		let item = cart.items.find(item => item.product.id === product.id);

		if (!item) {
			item = new CartItem();
			item.cart = cart;
			item.product = product;
			cart.items.push(item);
		}

		item.count = cartItem.count;

		const cartItemRepository = AppDataSource.getRepository(CartItem);
		await cartItemRepository.save(item);
		
		return cart;

	} catch (error) {
		throw error;
	}
};

export const deleteCart = async (userId: string): Promise<void> => {
	try {
		let existingCart: CartEntity | null = await CartRepository.getCartByUserId(userId);


		if (!existingCart) {
			console.log('no cart');
			return;
		}

		await CartRepository.deleteCart(existingCart);
		await CartRepository.addCart(userId);

	} catch (error) {
		throw error;
	}
};
