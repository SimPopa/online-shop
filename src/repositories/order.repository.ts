import { getRepository } from "typeorm";
import { OrderEntity } from "../models/models";
import { Order } from "../entities/Order";
import { AppDataSource } from "../data-source";

export const saveOrder = async (order: OrderEntity): Promise<OrderEntity> => {
    try {
        const orderRepository = AppDataSource.getRepository(Order);
        const newOrder = await orderRepository.save(order);
        return newOrder;
    } catch (error) {
        throw error;
    }
}
