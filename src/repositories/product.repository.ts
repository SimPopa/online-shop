import { getRepository } from "typeorm";
import { ProductEntity } from "../models/models";
import { Product } from "../entities/Product";
import { AppDataSource } from "../data-source";


export const getProductById = async (productId: string): Promise<Product | null> => {
    const productRepository = AppDataSource.getRepository(Product);
    try {
        const product = await productRepository.findOneBy({id: productId});
        return product;
    } catch (error) {
        throw error;
    }
}

export const getProducts = async (): Promise<ProductEntity[]> => {
    const productRepository = AppDataSource.getRepository(Product);
    try {
        const products = await productRepository.find();
        return products;
    } catch (error) {
        throw error;
    }
}