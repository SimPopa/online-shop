import { getRepository } from "typeorm";
import { CartEntity, CartItemEntity, ProductEntity } from "../models/models";
import { Cart } from "../entities/Cart";
import { CartItem } from "../entities/CartItem";
import { Product } from "../entities/Product";
import { AppDataSource } from "../data-source";


export const addCart = async (userId: string): Promise<CartEntity> => {
    const cartRepository = AppDataSource.getRepository(Cart);
    const cart = new Cart();
    cart.userId = userId;
    cart.isDeleted = false;
    await cartRepository.save(cart);
    return cart;
}

export const updateCart = async (cart: CartEntity): Promise<CartEntity> => {
    const cartRepository = AppDataSource.getRepository(Cart);
    const updatedCart = await cartRepository.save(cart);
    if (!updatedCart) {
        throw new Error("Cart not existing, please create");
    }
    return updatedCart;
}

export const getCartByUserId = async (userId: string): Promise<Cart | null> => {
    const cartRepository = AppDataSource.getRepository(Cart);
    let cart = await cartRepository.findOne({ 
        where: { userId, isDeleted: false },
        relations: ['items', 'items.product']});
    return cart;
}

export const deleteCart = async (cart: CartEntity): Promise<void> => {
    const cartRepository = AppDataSource.getRepository(Cart);
    cart.isDeleted = true;
    console.log(cart)
    const updatedCart = await cartRepository.save(cart);
    if (!updatedCart) {
        throw new Error("Cart not existing, please create");
    }
}