import winston from 'winston';
import path from 'path';
import fs from 'fs';

const logDirectory = process.env.LOG_DIR || path.join(process.cwd(), 'logs'); 

if (!fs.existsSync(logDirectory)) {
    fs.mkdirSync(logDirectory);
}

const logLevel = 'debug';

const logger = winston.createLogger({
    transports: [
        new winston.transports.Console({ level: logLevel }),
        new winston.transports.File({ filename: path.join(logDirectory, 'error.log'), level: 'error' }),
        new winston.transports.File({ filename: path.join(logDirectory, 'combined.log'), level: logLevel })
    ],
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.simple()
    )
});

export default logger;