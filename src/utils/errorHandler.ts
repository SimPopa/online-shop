import { NextFunction, Request, Response } from "express";
import logger from "./logger";

enum ErrorStatus {
    BadRequest = 400,
    NotFound = 404,
    Unauthorized = 401,
    Forbidden = 403,
    InternalServerError = 500
  }
  
  export const errorHandler = (err: Error, req: Request, res: Response, next: NextFunction) => {
    if (res.headersSent) {
      return next(err);
    }
  
    let statusCode: number;
  
    const errorName = err.name as keyof typeof ErrorStatus;
    const errorMessage = err.message;
  
    if (errorName && ErrorStatus[errorName]) {
      statusCode = ErrorStatus[errorName];
    } else {
      statusCode = 500;
    }

    logger.error(`Error occurred: ${errorMessage}`);
  
    const responseModel = {
      data: null,
      error: { message: errorMessage },
      stack: err.stack
    };
  
    return res.status(statusCode).json(responseModel);
  };