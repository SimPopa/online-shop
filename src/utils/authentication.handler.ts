import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";
import { User } from "../entities/User";
import { AppDataSource } from "../data-source";
import logger from "./logger";

declare global {
    namespace Express {
        interface Request {
            user?: User;
        }
    }
}

export const authenticationHandler = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const token = req.headers.authorization?.split(" ")[1];
        if (!token) {
            return res.status(401).json({ message: "Authorization token is missing" });
        }

        const decodedToken: any = jwt.verify(token, process.env.JWT_SECRET || "someverysecretstring");
        if (!decodedToken) {
            return res.status(401).json({ message: "Invalid token" });
        }

        const userRepository = AppDataSource.getRepository(User);
        const user = await userRepository.findOne({ where: { id: decodedToken.userId } });
        if (!user) {
            return res.status(403).json({ message: "User not found" });
        }

        req.user = user;

        next();
    } catch (error) {
        logger.error("Authentication error:", error);
        return res.status(500).json({ message: "Internal Server Error" });
    }
};

export const adminAuthorizationHandler = async (req: Request, res: Response, next: NextFunction) => {
    try {
		const user = req.user as User;
        if (user.role !== "admin") {
            return res.status(403).json({ message: "User is not authorized to access this resource" });
        }
        next();
    } catch (error) {
        logger.error("Authorization error:", error);
        return res.status(500).json({ message: "Internal Server Error" });
    }
};