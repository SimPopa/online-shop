import { Request, Response } from 'express';
import { ProductEntity, ResponseModel, } from '../models/models';
import * as ProductService from '../services/product.service';

export const getSingleProduct = async (req: Request, res: Response): Promise<void> => {
    try {
        const productId: string = req.params.productId;
        const product: ProductEntity | null = await ProductService.getSingleProduct(productId);
        if (product) {
            const response: ResponseModel<ProductEntity> = {
                data: product,
                error: null
            };
            res.status(200).json(response);
        } else {
            res.status(404).json({ error: 'No product with such id' });
        }
    } catch (error) {
        res.status(500).json({ error: (error as Error).message });
    }
};

export const getAllProducts = async (req: Request, res: Response): Promise<void> => {
    try {
        const products: ProductEntity[] = await ProductService.getProductsList();
        const response: ResponseModel<ProductEntity[]> = {
            data: products,
            error: null
        };
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({ error: (error as Error).message });
    }
};