import { Request, Response } from "express";
import { CartEntity, CartUpdateItemEntity, ResponseModel } from "../models/models";
import * as CartService from '../services/cart.service';
import Joi from 'joi';

const schema = Joi.object({
    productId: Joi.string().required(),
    count: Joi.number().integer().min(1).required()
});

export const getCart = async (req: Request, res: Response): Promise<void> => {
  try {
    const userId: string = req.user?.id as string;
    const cart: CartEntity = await CartService.getOrCreateUserCart(userId);
    const response: ResponseModel<CartEntity> = {
      data: cart,
      error: null
    };
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ error: { message: (error as Error).message } });
  }
};

export const updateCart = async (req: Request, res: Response): Promise<void> => {
  try {
    const userId: string = req.user?.id as string;
    const { error, value } = schema.validate(req.body);
    if (error) {
      throw new Error('Validation Error');
    }
    const cart: CartUpdateItemEntity = value as CartUpdateItemEntity;
    const updatedCart: CartEntity | null = await CartService.updateCart(userId, cart);
    const response: ResponseModel<CartEntity | null> = {
      data: updatedCart,
      error: null
    };
    res.status(200).json(response);
  } catch (error) {
    res.status(400).json({ error: { message: (error as Error).message } });
  }
}

export const deleteCart = async (req: Request, res: Response): Promise<void> => {
  try {
    const userId: string = req.user?.id as string;
    await CartService.deleteCart(userId);
    const response: ResponseModel<null> = {
      data: null,
      error: {
        message: 'success'
      }
    };
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ error: { message: (error as Error).message } });
  }
}