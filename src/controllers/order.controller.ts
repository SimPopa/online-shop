import { Request, Response } from "express";
import { OrderEntity, ResponseModel } from "../models/models";
import * as OrderService from '../services/order.service';

export const createOrder = async (req: Request, res: Response): Promise<void> => {
  try {
    const userId: string = req.user?.id as string;
    if (!Boolean(userId)) {
      throw new Error('User ID is missing');
    }

    const order: OrderEntity = await OrderService.createAnOrder(userId);

    const response: ResponseModel<OrderEntity> = {
      data: order,
      error: null
    };

    res.status(200).json(response);
  } catch (error) {
    const response: ResponseModel<null> = {
      data: null,
      error: {
        message: (error as Error).message || 'An error occurred while creating the order'
      }
    };

    res.status(500).json(response);
  }
};