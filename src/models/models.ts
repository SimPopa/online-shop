import mongoose, { Schema, Document } from "mongoose";

export interface UserEntity {
    id: string;
    email: string;
    password: string;
    role: string;
    name: string;
}

export interface ProductEntity {
    id: string;
    title: string;
    description: string;
    price: number;
}

const bookProduct: ProductEntity = {
    id: '51422fcd-0366-4186-ad5b-c23059b6f64f',
    title: 'Book',
    description: 'A very interesting book',
    price: 100
}


export interface CartItemEntity {
  product: ProductEntity;
  count: number;
}

export interface CartUpdateItemEntity {
  productId: string;
  count: number;
}

export interface CartEntity {
  id: string;
  userId: string;
  isDeleted: boolean;
  items: CartItemEntity[];
}

type ORDER_STATUS = 'created' | 'completed';

export interface OrderEntity {
  id: string,
  userId: string;
  cartId: string;
  items: CartItemEntity[]
  payment: {
    type: string,
    address?: any,
    creditCard?: any,
  },
  delivery: {
    type: string,
    address: any,
  },
  comments: string,
  status: ORDER_STATUS;
  total: number;
}

export interface ResponseModel<T> {
  data: T,
  error: ResponseError | null
}

interface ResponseError {
  message: string
}

const cartSchema = new Schema({
  userId: { type: String, required: true },
  isDeleted: { type: Boolean, default: false },
  items: [{ product: { type: Schema.Types.ObjectId, ref: 'Product' }, count: Number }]
});

export const CartModel = mongoose.model<CartEntity>('Cart', cartSchema);

export const productSchema = new mongoose.Schema<ProductEntity>({
  title: { type: String, required: true },
  price: { type: Number, required: true },
  description: { type: String, required: true }
});


export const ProductModel = mongoose.model<ProductEntity>("Product", productSchema);

export const orderSchema = new mongoose.Schema<OrderEntity>({
  userId: { type: String, required: true },
  cartId: { type: String, required: true },
  items: [{
    product: { type: Schema.Types.Mixed },
    count: { type: Number, required: true }
  }],
  payment: {
      type: { type: String, required: true },
      address: { type: mongoose.Schema.Types.Mixed },
      creditCard: { type: mongoose.Schema.Types.Mixed },
  },
  delivery: {
      type: { type: String, required: true },
      address: { type: mongoose.Schema.Types.Mixed, required: true },
  },
  comments: { type: String },
  status: { type: String, enum: ['created', 'completed'], default: 'created' },
  total: { type: Number, required: true }
});

export const OrderModel = mongoose.model<OrderEntity>("Order", orderSchema);