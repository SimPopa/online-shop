import { DataSource } from "typeorm";

import { Product } from "../entities/Product";
import { faker } from '@faker-js/faker';


export async function runMainSeeder(datasource: DataSource): Promise<any> {
  const productRepository = datasource.getRepository(Product);
  console.log(productRepository);

  const productsToCreate = [];
  for (let i = 0; i < 10; i++) {
    const product = new Product();
    product.title = faker.lorem.words(5);
    product.description = faker.lorem.paragraph(1);
    product.price = faker.number.float({min: 0, max: 300, fractionDigits: 2});
    console.log(product);
    productsToCreate.push(product);
  }

  await productRepository.save(productsToCreate);
}