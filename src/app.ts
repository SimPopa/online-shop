import dotenv from "dotenv";
dotenv.config();

import express, { Request, Response } from 'express';
import morgan from 'morgan';
import { adminAuthorizationHandler, authenticationHandler } from './utils/authentication.handler';
import { errorHandler } from './utils/errorHandler';
import * as ProductController from './controllers/product.controller';
import * as CartController from './controllers/cart.controller';
import * as OrderController from './controllers/order.controller';
import { Socket } from 'net';
import logger from './utils/logger';
import { AppDataSource } from './data-source';
import userRoutes from './user.routes';
import { runMainSeeder } from "./seeds/addProducts";

const app = express();
const { API_PORT } = process.env;
const port = API_PORT || 3000;

async function startServer() {
  try {
    await AppDataSource.initialize();

    app.use(express.json());
    app.use(morgan('dev'));

    app.use('/api/auth', userRoutes);
    app.use(authenticationHandler); 
    app.get('/health', (req: Request, res: Response) => {
      res.status(200).json({ message: 'Application is healthy' });
    });
    app.get('/api/products/:productId', ProductController.getSingleProduct);
    app.get('/api/products', ProductController.getAllProducts);
    app.get('/api/profile/cart', CartController.getCart);
    app.put('/api/profile/cart', CartController.updateCart);
    app.delete('/api/profile/cart', adminAuthorizationHandler, CartController.deleteCart);
    app.post('/api/profile/cart/checkout', OrderController.createOrder);
    app.use(errorHandler);

    const server = app.listen(port, () => {
      logger.info(`Server started on port ${port}`);
    });

    let connections: Socket[] = [];

    server.on('connection', (connection: Socket) => {
      logger.debug('New connection established');
      connections.push(connection);

      connection.on('close', () => {
        connections = connections.filter((currentConnection) => currentConnection !== connection);
        logger.debug('Connection closed');
      });
    });

    function shutdown() {
      logger.info('Received kill signal, shutting down gracefully');
      server.close(() => {
        logger.info('Closed out remaining connections');
        process.exit(0);
      });

      setTimeout(() => {
        logger.error('Could not close connections in time, forcefully shutting down');
        process.exit(1);
      }, 20000);

      connections.forEach((connection) => connection.end());

      setTimeout(() => {
        connections.forEach((connection) => connection.destroy());
      }, 10000);
    }

    process.on('SIGTERM', shutdown);
    process.on('SIGINT', shutdown);
  } catch (error) {
    logger.error('Error connecting to the database:', error);
    process.exit(1);
  }
}

startServer();
