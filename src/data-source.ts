import { DataSource } from 'typeorm';
import dotenv from 'dotenv';
import { User } from './entities/User';
import { Cart } from './entities/Cart';
import { CartItem } from './entities/CartItem';
import { Product } from './entities/Product';
import { Order } from './entities/Order';

dotenv.config();

const { DB_HOST, DB_PORT, DB_USER, DB_PASS, DB_DATABASE } = process.env;

export const AppDataSource = new DataSource({
  type: 'postgres',
  host: DB_HOST,
  port: parseInt(DB_PORT!, 10) || 5432,
  username: DB_USER,
  password: DB_PASS,
  database: DB_DATABASE,
  entities: [User, Cart, CartItem, Product, Order],
  migrations: ['dist/migrations/*.js'],
  synchronize: false,
  logging: true,
});
