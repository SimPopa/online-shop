import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, OneToOne } from "typeorm";
import { User } from "./User";
import { CartItem } from "./CartItem";
import { Order } from "./Order";

@Entity()
export class Cart {

    @PrimaryGeneratedColumn("uuid")
    id!: string;

    @Column()
    userId!: string;

    @Column()
    isDeleted!: boolean;

    @OneToMany(() => CartItem, cartItem => cartItem.cart)
    items!: CartItem[];

    @ManyToOne(() => User, user => user.carts)
    user!: User;

    @OneToOne(() => Order, order => order.cart)
    order!: Order;
}