import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { Cart } from "./Cart";
import { Order } from "./Order";
import bcrypt from "bcryptjs";

@Entity()
export class User {

    @PrimaryGeneratedColumn("uuid")
    id!: string;

    @Column()
    name!: string;

    @Column({ unique: true })
    email!: string;

    @Column()
    password!: string;

    @Column({ default: "user" })
    role!: string;

    @OneToMany(() => Cart, cart => cart.user)
    carts!: Cart[];

    @OneToMany(() => Order, order => order.user)
    orders!: Order[];

    async setPassword(password: string): Promise<void> {
        const hashedPassword = await bcrypt.hash(password, 10);
        this.password = hashedPassword;
    }

    async validatePassword(password: string): Promise<boolean> {
        return await bcrypt.compare(password, this.password);
    }
}