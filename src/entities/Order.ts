import { Column, Entity, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./User";
import { Cart } from "./Cart";
import { CartItem } from "./CartItem";

@Entity()
export class Order {

    @PrimaryGeneratedColumn('uuid')
    id!: string;

    @Column()
    userId!: string;

    @Column()
    cartId!: string;

    @OneToMany(() => CartItem, cartItem => cartItem.order)
    items!: CartItem[];

    @Column("jsonb", { nullable: true })
    payment!: { type: string, address?: any, creditCard?: any };

    @Column("jsonb")
    delivery!: { type: string, address: any };

    @Column()
    comments!: string;

    @Column()
    status!: string;

    @Column("decimal", { precision: 10, scale: 2 })
    total!: number;

    @ManyToOne(() => User, user => user.orders)
    user!: User;

    @OneToOne(() => Cart, cart => cart.order)
    cart!: Cart;
}