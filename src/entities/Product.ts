import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { CartItem } from "./CartItem";

@Entity()
export class Product {

    @PrimaryGeneratedColumn("uuid")
    id!: string;

    @Column()
    title!: string;

    @Column()
    description!: string;

    @Column("decimal", { precision: 10, scale: 2 })
    price!: number;
    
    @OneToMany(() => CartItem, cartItem => cartItem.product)
    cartItems!: CartItem[];
}