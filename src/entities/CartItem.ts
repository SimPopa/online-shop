import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { Product } from "./Product";
import { Cart } from "./Cart";
import { Order } from "./Order";

@Entity()
export class CartItem {
    [x: string]: any;

    @PrimaryGeneratedColumn()
    id!: number;

    @ManyToOne(() => Product, product => product.cartItems)
    product!: Product;

    @Column()
    count!: number;

    @ManyToOne(() => Cart, cart => cart.items)
    cart!: Cart;

    @ManyToOne(() => Order, order => order.items)
    order!: Order;
}