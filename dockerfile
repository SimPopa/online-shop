# Use an official Node.js runtime as a parent image
FROM node:20.1.0

# Set the working directory
WORKDIR /app

# Copy package.json and package-lock.json to the container
COPY package*.json ./

# Install dependencies
RUN npm install

# Install pm2 globally
RUN npm install -g pm2

# Copy the rest of the application code to the container
COPY . .

# Build the application
RUN npm run build

# Expose port 3000
EXPOSE 3000

# Start the app with pm2-runtime
CMD ["pm2-runtime", "start", "dist/app.js", "--name", "osh-network-app"]
